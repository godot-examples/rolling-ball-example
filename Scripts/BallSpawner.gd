# Godot Engine v3.3.2.stable.official

extends Spatial


var ball_scene = preload("res://Scenes/Ball.tscn")


func _input(event):
	if event.is_action_pressed("ui_accept"):
		# Create a new ball.
		var new_ball = ball_scene.instance()
		
		# Move the ball to our position.
		new_ball.transform = transform
		
		# Add the ball to the scene.
		get_parent().add_child(new_ball)
		
		# Give the ball momentum in the direction of the loop.
		new_ball.apply_central_impulse(Vector3(-1.11 * new_ball.weight, 0.0, 0.0))
